# Share IT (Wifi File Transfer) open-source file and text sharing application (built on top of TrebleShot)
Send and receive files over available connections with functionalities like
being able to resume transfer when an error occurred. [FAQ](https://gitlab.com/sabinbajracharya/send-it/blob/master/FAQ.md).

## Get it on
[<img src="assets/google-play-badge.png" width="230">](https://play.google.com/store/apps/details?id=co.senditnow.fast)

## Main features
* Pause and resume transfers even when an error is given
* Share multiple content from your video, photo, music and application libraries
* No internet connection is required: Set up a hotspot and connect to it using a QR code
* Share with multiple devices at the same time
* Share texts of any kind and save them to TrebleShot
* Share folders as they are for backup or any other purpose
* Light UI: Works faster than its rivals on low-end devices
* Speed-oriented: Minimal UI focusing on and optimized for speed
* Professional features: Network change handling, choosing network after speed measurement
* It is secure by allowing you to give extra permission to or block a device

## Build from source notes
Required tools;
* Build tools: 28.0.3
* Support library: 1.0.0 - androidx
* Gradle plugin: 3.2.1
* Android Studio: 3.2.1

## Screenshots
[<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/shot_1.png" width=160>](fastlane/metadata/android/en-US/images/phoneScreenshots/shot_1.png)
[<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/shot_2.png" width=160>](fastlane/metadata/android/en-US/images/phoneScreenshots/shot_2.png)
[<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/shot_3.png" width=160>](fastlane/metadata/android/en-US/images/phoneScreenshots/shot_3.png)
[<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/shot_4.png" width=160>](fastlane/metadata/android/en-US/images/phoneScreenshots/shot_4.png)
[<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/shot_5.png" width=160>](fastlane/metadata/android/en-US/images/phoneScreenshots/shot_5.png)
[<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/shot_6.png" width=160>](fastlane/metadata/android/en-US/images/phoneScreenshots/shot_6.png)
[<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/shot_7.png" width=160>](fastlane/metadata/android/en-US/images/phoneScreenshots/shot_7.png)
[<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/shot_8.png" width=160>](fastlane/metadata/android/en-US/images/phoneScreenshots/shot_8.png)
[<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/shot_9.png" width=160>](fastlane/metadata/android/en-US/images/phoneScreenshots/shot_9.png)

## Supported languages
Arabic, Basque, Chinese (Simplified), Chinese (Traditional), Dutch, English, French, German, Hebrew,
Italian, Japanese, Norwegian Bokmål, Polish, Russian, Spanish, Swedish, Turkish

Go to the [translation page](https://hosted.weblate.org/engage/TrebleShot/) on Weblate to see all available languages, or to start a new translation. If you want to contribute to a language for the first time, please read [this wiki](https://github.com/genonbeta/TrebleShot/wiki/Language-contribution). Languages are included here once they pass 87.5% translation threshold.

This application is licensed under GPL-2.0+  
