package com.genonbeta.TrebleShot.util

import android.view.View
import com.genonbeta.TrebleShot.BuildConfig
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView

fun AdView.startAd() {
    this@startAd.visibility = View.GONE
    adListener = object : AdListener() {
        override fun onAdLoaded() {
            super.onAdLoaded()
            this@startAd.visibility = View.VISIBLE
        }
    }

    if (BuildConfig.DEBUG) {
        loadAd(AdRequest.Builder().addTestDevice("2EFC963B2DE3E05AC95AA9870B4A63B2").build())
    } else {
        loadAd(AdRequest.Builder().build())
    }
}