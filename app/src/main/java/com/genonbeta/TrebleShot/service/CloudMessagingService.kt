package com.genonbeta.TrebleShot.service

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import android.app.NotificationManager
import android.content.Context
import android.media.RingtoneManager
import androidx.core.app.NotificationCompat
import kotlin.random.Random
import android.app.NotificationChannel
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import com.genonbeta.TrebleShot.R


class CloudMessagingService : FirebaseMessagingService() {

    companion object {
        private const val NOTIFICATION_ID_EXTRA = "notificationId"
        private const val IMAGE_URL_EXTRA = "imageUrl"
        private const val ADMIN_CHANNEL_ID = "sendit_channel"
        private const val TAG = "CloudMessagingService"
    }

    private lateinit var notificationManager: NotificationManager

    override fun onNewToken(token: String?) {
        super.onNewToken(token)
        Log.e(TAG, token)
    }
    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        //Setting up Notification channels for android O and above
        Log.e(TAG, remoteMessage?.data?.toString() ?: "onMessageReceived")
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            setupChannels()
        }
        val notificationId = Random.nextInt(60000)

        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)  //a resource for your custom small icon
                .setContentTitle(remoteMessage?.data?.get("title")) //the "title" value you sent in your notification
                .setContentText(remoteMessage?.data?.get("message")) //ditto
                .setAutoCancel(true)  //dismisses the notification on click
                .setSound(defaultSoundUri)

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(notificationId /* ID of notification */, notificationBuilder.build())
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun setupChannels() {
        val adminChannelName = getString(R.string.notifications_channel_name)
        val adminChannelDescription = getString(R.string.notifications_channel_description)

        val adminChannel: NotificationChannel
        adminChannel = NotificationChannel(ADMIN_CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_LOW)
        adminChannel.description = adminChannelDescription
        adminChannel.enableLights(true)
        adminChannel.lightColor = R.color.colorSecondary
        adminChannel.enableVibration(true)
        notificationManager.createNotificationChannel(adminChannel)
    }
}