package com.genonbeta.TrebleShot.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.genonbeta.TrebleShot.R
import kotlinx.android.synthetic.main.activity_privacy_policy.*


class PrivacyPolicy : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_privacy_policy)
        supportActionBar?.title = resources.getString(R.string.butn_privacy_policy)
        markdownView.loadMarkdownFile("https://gitlab.com/sabinbajracharya/send-it/raw/master/Privacy.md")
    }
}
