package com.genonbeta.TrebleShot.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.genonbeta.TrebleShot.R
import com.genonbeta.TrebleShot.activity.AboutActivity
import com.genonbeta.TrebleShot.activity.ManageDevicesActivity
import com.genonbeta.TrebleShot.activity.PreferencesActivity
import com.genonbeta.TrebleShot.activity.ShareActivity
import com.genonbeta.TrebleShot.app.Activity
import com.genonbeta.TrebleShot.service.CommunicationService
import com.genonbeta.TrebleShot.service.DeviceScannerService
import com.genonbeta.TrebleShot.service.WorkerService
import com.genonbeta.TrebleShot.ui.callback.IconSupport
import com.genonbeta.TrebleShot.ui.callback.TitleSupport
import com.genonbeta.TrebleShot.util.AppUtils
import com.genonbeta.TrebleShot.util.AppUtils.getDefaultPreferences
import com.genonbeta.TrebleShot.util.FileUtils
import com.genonbeta.android.framework.app.Fragment
import com.genonbeta.android.framework.io.DocumentFile
import com.genonbeta.android.framework.util.Interrupter
import kotlinx.android.synthetic.main.fragment_more_list.*
import kotlinx.android.synthetic.main.header_default_device.*
import java.io.File

class MoreListFragment : Fragment(), IconSupport, TitleSupport {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_more_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        createHeaderView()
        setupActions()
    }

    override fun getIconRes() = R.drawable.ic_menu_more

    override fun getTitle(context: Context?) = context?.getString(R.string.text_more)

    private fun setupActions() {
        menu_activity_main_manage_devices.setOnClickListener { startActivity(Intent(context, ManageDevicesActivity::class.java)) }
        menu_activity_main_about.setOnClickListener { startActivity(Intent(context, AboutActivity::class.java)) }
        menu_activity_feedback.setOnClickListener { AppUtils.createFeedbackIntent(activity) }
        menu_activity_main_preferences.setOnClickListener { startActivity(Intent(context, PreferencesActivity::class.java)) }
        menu_activity_main_send_application.setOnClickListener { sendThisApplication() }
        menu_activity_main_exit.setOnClickListener {
            (context as? Activity)?.apply {
                stopService(Intent(this, CommunicationService::class.java))
                stopService(Intent(this, DeviceScannerService::class.java))
                stopService(Intent(this, WorkerService::class.java))
                finish()
            }
        }
    }

    private fun sendThisApplication() {
        Handler(Looper.myLooper()).post {
            try {
                context?.apply {
                    val interrupter = Interrupter()
                    val pm = packageManager
                    val packageInfo = pm.getPackageInfo(applicationInfo.packageName, 0)
                    val fileName = "${packageInfo.applicationInfo.loadLabel(pm)}_${packageInfo.versionName}.apk"
                    val storageDirectory = FileUtils.getApplicationDirectory(applicationContext, getDefaultPreferences(this))
                    val codeFile = DocumentFile.fromFile(File(applicationInfo.sourceDir))
                    val cloneFile = storageDirectory.createFile(null, FileUtils.getUniqueFileName(storageDirectory, fileName, true))

                    FileUtils.copy(this, codeFile, cloneFile, interrupter)

                    try {
                        val sendIntent = Intent(Intent.ACTION_SEND)
                                .putExtra(ShareActivity.EXTRA_FILENAME_LIST, fileName)
                                .putExtra(Intent.EXTRA_STREAM, FileUtils.getSecureUri(this, cloneFile))
                                .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                                .setType(cloneFile.type)

                        startActivity(Intent.createChooser(sendIntent, getString(R.string.text_fileShareAppChoose)))
                    } catch (e: IllegalArgumentException) {
                        Toast.makeText(this, R.string.mesg_providerNotAllowedError, Toast.LENGTH_LONG).show()
                        e.printStackTrace()
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun createHeaderView() {
        (context as? Activity)?.apply {
            val localDevice = AppUtils.getLocalDevice(applicationContext)
            header_default_device_name_text.text = localDevice.nickname
            header_default_device_version_text.text = localDevice.versionName
            loadProfilePictureInto(localDevice.nickname, header_default_device_image)
            header_default_device_edit_image.setOnClickListener { startProfileEditor() }
        }
    }
}
