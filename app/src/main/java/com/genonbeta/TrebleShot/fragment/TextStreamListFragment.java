package com.genonbeta.TrebleShot.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.genonbeta.CoolSocket.CoolSocket;
import com.genonbeta.TrebleShot.R;
import com.genonbeta.TrebleShot.activity.ConnectionManagerActivity;
import com.genonbeta.TrebleShot.activity.ShareActivity;
import com.genonbeta.TrebleShot.activity.TextEditorActivity;
import com.genonbeta.TrebleShot.adapter.TextStreamListAdapter;
import com.genonbeta.TrebleShot.app.Activity;
import com.genonbeta.TrebleShot.app.EditableListFragment;
import com.genonbeta.TrebleShot.app.EditableListFragmentImpl;
import com.genonbeta.TrebleShot.app.GroupEditableListFragment;
import com.genonbeta.TrebleShot.config.Keyword;
import com.genonbeta.TrebleShot.database.AccessDatabase;
import com.genonbeta.TrebleShot.object.NetworkDevice;
import com.genonbeta.TrebleShot.object.TextStreamObject;
import com.genonbeta.TrebleShot.service.WorkerService;
import com.genonbeta.TrebleShot.ui.UIConnectionUtils;
import com.genonbeta.TrebleShot.ui.callback.IconSupport;
import com.genonbeta.TrebleShot.ui.callback.SharingActionModeCallback;
import com.genonbeta.TrebleShot.ui.callback.TitleSupport;
import com.genonbeta.TrebleShot.util.AppUtils;
import com.genonbeta.TrebleShot.util.CommunicationBridge;
import com.genonbeta.TrebleShot.util.Debouncer;
import com.genonbeta.TrebleShot.widget.GroupEditableListAdapter;
import com.genonbeta.android.framework.widget.PowerfulActionMode;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.RecyclerView;

import static com.genonbeta.TrebleShot.activity.TextEditorActivity.JOB_SEND_TEXT;
import static com.genonbeta.TrebleShot.activity.TextEditorActivity.REQUEST_CODE_CHOOSE_DEVICE;

/**
 * created by: Veli
 * date: 30.12.2017 13:25
 */

public class TextStreamListFragment
		extends GroupEditableListFragment<TextStreamObject, GroupEditableListAdapter.GroupViewHolder, TextStreamListAdapter>
		implements IconSupport, TitleSupport
{
	private StatusReceiver mStatusReceiver = new StatusReceiver();

	private TextStreamObject mTextStreamObject;
	private EditText userMessage;
	private Debouncer debouncer = new Debouncer();

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setFilteringSupported(true);
		setDefaultOrderingCriteria(TextStreamListAdapter.MODE_SORT_ORDER_DESCENDING);
		setDefaultSortingCriteria(TextStreamListAdapter.MODE_SORT_BY_DATE);
		setDefaultGroupingCriteria(TextStreamListAdapter.MODE_GROUP_BY_DATE);
		setDefaultSelectionCallback(new SelectionCallback(this));
	}

	@Override
	protected RecyclerView onListView(View mainContainer, ViewGroup listViewContainer)
	{
		LinearLayout view = (LinearLayout) getLayoutInflater().inflate(R.layout.layout_text_stream, null, false);
//		FloatingActionButton actionButton = view.findViewById(R.id.layout_text_stream_fab);

		listViewContainer.addView(view);

//		actionButton.setOnClickListener(new View.OnClickListener()
//		{
//			@Override
//			public void onClick(View v)
//			{
//				startActivity(new Intent(getActivity(), TextEditorActivity.class)
//						.setAction(TextEditorActivity.ACTION_EDIT_TEXT));
//			}
//		});

		FloatingActionButton sendButton = view.findViewById(R.id.button_chatbox_send);
		userMessage = view.findViewById(R.id.edittext_chatbox);

		sendButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
			    if (userMessage.getText().toString().isEmpty()) return;
				startActivityForResult(
						new Intent(getActivity(), ConnectionManagerActivity.class)
								.putExtra(ConnectionManagerActivity.EXTRA_REQUEST_TYPE, ConnectionManagerActivity.RequestType.RETURN_RESULT.toString())
								.putExtra(ConnectionManagerActivity.EXTRA_ACTIVITY_SUBTITLE, getString(R.string.text_sendText)),
						REQUEST_CODE_CHOOSE_DEVICE);
			}
		});

		return super.onListView(mainContainer, (FrameLayout) view.findViewById(R.id.layout_text_stream_content));
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);

		setEmptyImage(R.drawable.ic_forum_white_24dp);
		setEmptyText(getString(R.string.text_listEmptyTextStream));
		getListView().setClipToPadding(false);
		getListView().setPadding(0,0,0, (int) (getResources().getDimension(R.dimen.fab_margin) * 6));
	}

	@Override
	public void onSortingOptions(Map<String, Integer> options)
	{
		options.put(getString(R.string.text_sortByName), TextStreamListAdapter.MODE_SORT_BY_NAME);
		options.put(getString(R.string.text_sortByDate), TextStreamListAdapter.MODE_SORT_BY_DATE);
	}

	@Override
	public void onGroupingOptions(Map<String, Integer> options)
	{
		options.put(getString(R.string.text_groupByNothing), TextStreamListAdapter.MODE_GROUP_BY_NOTHING);
		options.put(getString(R.string.text_groupByDate), TextStreamListAdapter.MODE_GROUP_BY_DATE);
	}

	@Override
	public int onGridSpanSize(int viewType, int currentSpanSize)
	{
		return viewType == TextStreamListAdapter.VIEW_TYPE_REPRESENTATIVE
				? currentSpanSize
				: super.onGridSpanSize(viewType, currentSpanSize);
	}

	@Override
	public TextStreamListAdapter onAdapter()
	{
		final AppUtils.QuickActions<GroupEditableListAdapter.GroupViewHolder> quickActions = new AppUtils.QuickActions<GroupEditableListAdapter.GroupViewHolder>()
		{
			@Override
			public void onQuickActions(final GroupEditableListAdapter.GroupViewHolder clazz)
			{
				if (!clazz.isRepresentative())
					registerLayoutViewClicks(clazz);
			}
		};

		return new TextStreamListAdapter(getActivity(), AppUtils.getDatabase(getContext()))
		{
			@NonNull
			@Override
			public GroupViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
			{
				return AppUtils.quickAction(super.onCreateViewHolder(parent, viewType), quickActions);
			}
		};
	}

	@Override
	public boolean onDefaultClickAction(GroupEditableListAdapter.GroupViewHolder holder)
	{
		try {
			TextStreamObject object = getAdapter().getItem(holder.getAdapterPosition());

			startActivity(new Intent(getContext(), TextEditorActivity.class)
					.setAction(TextEditorActivity.ACTION_EDIT_TEXT)
					.putExtra(TextEditorActivity.EXTRA_CLIPBOARD_ID, object.id)
					.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

			return true;
		} catch (Exception e) {
		}

		return false;
	}

	@Override
	public void onResume()
	{
		super.onResume();

		getActivity().registerReceiver(mStatusReceiver, new IntentFilter(AccessDatabase.ACTION_DATABASE_CHANGE));
		refreshList();
		scrollToBottom();
	}

	@Override
	public void onPause()
	{
		super.onPause();
		getActivity().unregisterReceiver(mStatusReceiver);
	}

	@Override
	public int getIconRes()
	{
		return R.drawable.ic_short_text_white_24dp;
	}

	@Override
	public CharSequence getTitle(Context context)
	{
		return context.getString(R.string.text_textStream);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == android.app.Activity.RESULT_OK) {
			if (requestCode == REQUEST_CODE_CHOOSE_DEVICE
					&& data != null
					&& data.hasExtra(ConnectionManagerActivity.EXTRA_DEVICE_ID)
					&& data.hasExtra(ConnectionManagerActivity.EXTRA_CONNECTION_ADAPTER)) {
				String deviceId = data.getStringExtra(ConnectionManagerActivity.EXTRA_DEVICE_ID);
				String connectionAdapter = data.getStringExtra(ConnectionManagerActivity.EXTRA_CONNECTION_ADAPTER);

				try {
					NetworkDevice networkDevice = new NetworkDevice(deviceId);
					NetworkDevice.Connection connection = new NetworkDevice.Connection(deviceId, connectionAdapter);

					((Activity)getActivity()).getDatabase().reconstruct(networkDevice);
					((Activity)getActivity()).getDatabase().reconstruct(connection);

					doCommunicate(networkDevice, connection);
				} catch (Exception e) {
					Toast.makeText(getActivity(), R.string.mesg_somethingWentWrong, Toast.LENGTH_SHORT).show();
				}
			}
		}
	}

	protected void doCommunicate(final NetworkDevice device, final NetworkDevice.Connection connection)
	{
		createSnackbar(R.string.mesg_communicating).show();

		WorkerService.run(getActivity(), new WorkerService.RunningTask(TAG, JOB_SEND_TEXT)
		{
			@Override
			public void onRun()
			{
				final DialogInterface.OnClickListener retryButtonListener = new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						doCommunicate(device, connection);
					}
				};

				CommunicationBridge.connect(((Activity)getActivity()).getDatabase(), true, new CommunicationBridge.Client.ConnectionHandler()
				{
					@Override
					public void onConnect(CommunicationBridge.Client client)
					{
						client.setDevice(device);

						try {
							final JSONObject jsonRequest = new JSONObject();
							final CoolSocket.ActiveConnection activeConnection = client.communicate(device, connection);

							jsonRequest.put(Keyword.REQUEST, Keyword.REQUEST_CLIPBOARD);
							String message = userMessage != null ? userMessage.getText().toString() : "Unable to get Message";
							jsonRequest.put(Keyword.TRANSFER_CLIPBOARD_TEXT, message);

							activeConnection.reply(jsonRequest.toString());

							CoolSocket.ActiveConnection.Response response = activeConnection.receive();
							activeConnection.getSocket().close();

							JSONObject clientResponse = new JSONObject(response.response);

							if (clientResponse.has(Keyword.RESULT) && clientResponse.getBoolean(Keyword.RESULT)) {
								createSnackbar(R.string.mesg_sent).show();
								saveText();
								clearUserMessage();
							} else
								UIConnectionUtils.showConnectionRejectionInformation(
										getActivity(),
										device, clientResponse, retryButtonListener);
						} catch (Exception e) {
							e.printStackTrace();
							UIConnectionUtils.showUnknownError(getActivity(), retryButtonListener);
						}
					}
				});
			}
		});
	}

	private void clearUserMessage() {
		if (userMessage != null && getActivity() != null)
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					userMessage.setText("");
					//getListView().smoothScrollToPosition(getAdapter().getCount() - 1);
				}
			});
	}

	private void saveText()
	{
		mTextStreamObject = new TextStreamObject(AppUtils.getUniqueNumber());
		mTextStreamObject.date = System.currentTimeMillis();
		mTextStreamObject.text = userMessage != null ? userMessage.getText().toString() : "Unable to get Message";

		if (getActivity() != null) {
			((Activity) getActivity()).getDatabase().publish(mTextStreamObject);
		}
	}

	private void scrollToBottom() {
//		debouncer.debounce(new Runnable() {
//			@Override
//			public void run() {
//				if (getAdapter().getCount() > 0) {
//					getListView().smoothScrollToPosition(getAdapter().getCount() - 1);
//				}
//			}
//		}, 500);
	}

	private class StatusReceiver extends BroadcastReceiver
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if (AccessDatabase.ACTION_DATABASE_CHANGE.equals(intent.getAction())
					&& intent.hasExtra(AccessDatabase.EXTRA_TABLE_NAME)
					&& intent.getStringExtra(AccessDatabase.EXTRA_TABLE_NAME).equals(AccessDatabase.TABLE_CLIPBOARD)) {
				refreshList();
				scrollToBottom();
			}
		}
	}

	private static class SelectionCallback extends EditableListFragment.SelectionCallback<TextStreamObject>
	{
		public SelectionCallback(EditableListFragmentImpl<TextStreamObject> fragment)
		{
			super(fragment);
		}

		@Override
		public boolean onCreateActionMenu(Context context, PowerfulActionMode actionMode, Menu menu)
		{
			super.onCreateActionMenu(context, actionMode, menu);
			actionMode.getMenuInflater().inflate(R.menu.action_mode_text_stream, menu);
			return true;
		}

		@Override
		public boolean onActionMenuItemSelected(Context context, PowerfulActionMode actionMode, MenuItem item)
		{
			int id = item.getItemId();

			ArrayList<TextStreamObject> selectionList = getFragment().getSelectionConnection().getSelectedItemList();

			if (id == R.id.action_mode_text_stream_delete) {
				AppUtils.getDatabase(getFragment().getContext()).remove(selectionList);
			} else if (id == R.id.action_mode_share_all_apps || id == R.id.action_mode_share_trebleshot) {
				if (selectionList.size() == 1) {
					TextStreamObject streamObject = selectionList.get(0);

					Intent shareIntent = new Intent(item.getItemId() == R.id.action_mode_share_all_apps
							? Intent.ACTION_SEND : ShareActivity.ACTION_SEND)
							.putExtra(Intent.EXTRA_TEXT, streamObject.text)
							.setType("text/*");

					getAdapter().getContext().startActivity((item.getItemId() == R.id.action_mode_share_all_apps) ? Intent.createChooser(shareIntent, getFragment().getContext().getString(R.string.text_fileShareAppChoose)) : shareIntent);
				} else {
					Toast.makeText(context, R.string.mesg_textShareLimit, Toast.LENGTH_SHORT).show();
					return false;
				}
			} else
				return super.onActionMenuItemSelected(context, actionMode, item);

			return true;
		}
	}
}